import 'package:flutter/material.dart';
import 'package:task_management/bloc/home_bloc_provider.dart';
import 'package:task_management/ui/home_page.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Task Management',
      theme: ThemeData(),
      home: HomeBlocProvider(child: HomePage()),
    );
  }
}
