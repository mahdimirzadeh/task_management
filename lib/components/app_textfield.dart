import 'package:flutter/material.dart';

class AppTextField extends StatelessWidget {
  final String hint;
  final int? maxLine;
  final TextEditingController controller;
  const AppTextField(
      {super.key, required this.hint, this.maxLine, required this.controller});

  @override
  Widget build(BuildContext context) {
    return TextField(
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        hintText: hint,
        labelText: hint,
      ),
      controller: controller,
      maxLines: maxLine,
    );
  }
}
