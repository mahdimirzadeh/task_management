import 'package:flutter/material.dart';
import 'package:task_management/bloc/home_bloc.dart';
import 'package:task_management/bloc/home_bloc_provider.dart';
import 'package:task_management/components/app_textfield.dart';

class TaskDialog extends StatelessWidget {
  final titleController = TextEditingController();
  final descriptionController = TextEditingController();
  final BuildContext context;

  TaskDialog({super.key, required this.context});

  @override
  Widget build(BuildContext ctx) {
    return AlertDialog(
      title: Text("Insert Task"),
      content: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          AppTextField(
            hint: 'Title',
            controller: titleController,
          ),
          const SizedBox(
            height: 12,
          ),
          AppTextField(
            hint: 'Description',
            maxLine: 4,
            controller: descriptionController,
          ),
        ],
      ),
      actions: [
        TextButton(
            onPressed: () {
              Navigator.of(context).pop();
            },
            child: Text("Cancel")),
        ElevatedButton(
            onPressed: () {
              HomeBlocProvider.of(context)
                  .insertTask(titleController.text, descriptionController.text);
              Navigator.of(context).pop();
            },
            child: Text("Save")),
      ],
    );
  }
}
