class Task {
  String title;
  String description;
  bool isCompleted = false;
  Task({
    required this.title,
    required this.description,
  });
}
