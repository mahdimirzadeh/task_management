import 'package:rxdart/subjects.dart';
import 'package:task_management/data/model/task.dart';

class HomeBloc {
  final _tasksSubject = PublishSubject<List<Task>>();
  Stream<List<Task>> get tasksSubject => _tasksSubject.stream;
  List<Task> list = [];

  insertTask(String title, String description) {
    final task = Task(title: title, description: description);
    list.add(task);
    _tasksSubject.sink.add(list);
  }

  updateTask(int index, bool isCompleted) {
    list[index].isCompleted = isCompleted;
    _tasksSubject.sink.add(list);
  }

  filter(TaskFilter filter) {
    switch (filter) {
      case TaskFilter.all:
        _tasksSubject.sink.add(list);
        break;
      case TaskFilter.completed:
        final result = list.where((element) => element.isCompleted).toList();
        _tasksSubject.sink.add(result);
        break;
      case TaskFilter.notCompleted:
        final result = list.where((element) => !element.isCompleted).toList();
        _tasksSubject.sink.add(result);
        break;
    }
  }

  sort(int oldIndex, int newIndex) {
    if (oldIndex < newIndex) {
      newIndex -= 1;
    }
    final item = list.removeAt(oldIndex);
    list.insert(newIndex, item);
    _tasksSubject.sink.add(list);
  }

  void dispose() {
    _tasksSubject.close();
  }
}

enum TaskFilter { all, completed, notCompleted }
