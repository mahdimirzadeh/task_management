import 'package:flutter/material.dart';
import 'package:task_management/bloc/home_bloc.dart';

class HomeBlocProvider extends InheritedWidget {
  final HomeBloc bloc;

  HomeBlocProvider({Key? key, required Widget child})
      : bloc = HomeBloc(),
        super(key: key, child: child);

  @override
  bool updateShouldNotify(covariant HomeBlocProvider oldWidget) {
    return bloc != oldWidget.bloc;
  }

  static HomeBloc of(BuildContext context) {
    return (context.dependOnInheritedWidgetOfExactType<HomeBlocProvider>()
            as HomeBlocProvider)
        .bloc;
  }
}
