import 'package:flutter/material.dart';
import 'package:task_management/bloc/home_bloc.dart';
import 'package:task_management/bloc/home_bloc_provider.dart';
import 'package:task_management/components/task_dialog.dart';
import 'package:task_management/components/task_item.dart';
import 'package:task_management/data/model/task.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Tasks"),
        centerTitle: false,
        actions: [
          PopupMenuButton<TaskFilter>(
            onSelected: (value) {
              HomeBlocProvider.of(context).filter(value);
            },
            itemBuilder: (context) {
              return [
                PopupMenuItem(
                  child: Text('All'),
                  value: TaskFilter.all,
                ),
                PopupMenuItem(
                    child: Text('Completed'), value: TaskFilter.completed),
                PopupMenuItem(
                    child: Text('Not Completed'),
                    value: TaskFilter.notCompleted),
              ];
            },
          ),
        ],
      ),
      body: StreamBuilder<List<Task>>(
          stream: HomeBlocProvider.of(context).tasksSubject,
          builder: (context, snapshot) {
            if (snapshot.data == null) {
              return const Center(child: Text('List is Empty'));
            }
            if (snapshot.data!.isEmpty) {
              return const Center(child: Text('Selected Filter is empty'));
            }
            return ReorderableListView(
              children: [
                for (int index = 0; index < snapshot.data!.length; index++)
                  TaskItem(
                    key: ValueKey('$index'),
                    task: snapshot.data![index],
                    onChanged: (bool? value) {
                      HomeBlocProvider.of(context)
                          .updateTask(index, value ?? false);
                    },
                  )
              ],
              onReorder: (oldIndex, newIndex) {
                HomeBlocProvider.of(context).sort(oldIndex, newIndex);
              },
            );
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
            context: context,
            builder: (ctx) {
              return TaskDialog(
                context: context,
              );
            },
          );
        },
        child: const Icon(Icons.add_rounded),
      ),
    );
  }
}
