import 'package:flutter_test/flutter_test.dart';
import 'package:task_management/bloc/home_bloc.dart';

void main() {
  test('if insert a task the list has 1 lenght', () {
    final bloc = HomeBloc();
    bloc.insertTask('Test', 'Test');
    expect(bloc.list.length, 1);
  });
  test('if filter set to completed and no any completed subject has zero',
      () async {
    final bloc = HomeBloc();
    bloc.insertTask('Test', 'Test');

    expectLater(bloc.tasksSubject, emits([]));
    bloc.filter(TaskFilter.completed);
  });
}
