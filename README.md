# Task Management

Code challenge with bloc architecture and testing

# Features
- Add Task
- Sort Task with hold and drag
- Filter list (All,Completed,Not Completed)
- Mark as done for each task

# Tech
- Implement code with RxDart
- Add Inherited Widget for HomeBloc file because I don't use Dependency injection and I keep every thing simple.
- Add unit testing
- Add Widget testing 
